# Include my bin dir on the PATH
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/Documents/bin:$PATH
export XDG_CONFIG_HOME=$HOME/.config/

# Launch sway if signing in on tty1
if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
	export MOZ_ENABLE_WAYLAND=1
	export XDG_CURRENT_DESKTOP=sway
	export XDG_SESSION_TYPE=wayland
	exec sway
fi
